var config = {

//--------------------------------------------------------------------
// Webinterface configuration
// The port which the interface is hosted at
    port: 8080,

//--------------------------------------------------------------------
// `node-dns` configuration:
// Hostname of the server(s), multiple supported via DNS
    dns_hostname: "127.0.0.1",
// Port of the server(s)
    dns_port: 8011,

//--------------------------------------------------------------------
// `slicer-server` configuration:
// Hostname of the server(s), multiple supported via DNS
    slicer_hostname: "iomoss.dk",
// Port of the server(s)
    slicer_port: 8000,
// Ping frequency
// Note: Every 5 seconds
    slicer_ping_frequency: 5000,
// DNS recheck frequency
// Note: Once per hour
    slicer_dns_recheck_frequency: 3600000,

//--------------------------------------------------------------------
// Logging configuration
// The lowest error-severity to see
    log_level: 7

}

module.exports = config;
