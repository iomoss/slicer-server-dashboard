#!/bin/bash

# Clean-up any old leftovers
rm -rf deploy

# Generate the /build folder
npm run release

# Setup the deployment folder for tar.gz'ing
mkdir -p deploy

# We need everything from build (js files)
cp -r build deploy/
# ... and every html file from the src tree
cp src/index.html deploy/index.html

# Then we're ready to tar the result
#tar -czvf deploy.tar.gz -C deploy .

# And clean up after ourselves
#rm -rf deploy
