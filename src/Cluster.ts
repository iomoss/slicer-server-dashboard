import {Component, View, bootstrap} from 'angular2/angular2';
import {Http} from 'angular2/http';
import {HTTP_BINDINGS} from 'angular2/http';

var config = require('../config/config');

// Annotation section
@Component({
    selector: 'cluster'
})
@View({
    template: `
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

        <div>
		<h3>Slicer servers</h3>
		<ul>  
			<li *ng-for="#server of servers">
				<i class="fa {{ server.logo }}"></i>
				{{ server.name }} <br />
				IP: {{ server.ip }} <br />
				STATUS:	<i class="fa {{ server.online_logo }}"></i>
			</li>
		<ul>  
        </div>
    `
})
// Component controller
export class Cluster 
{
	private http: Http;
	private servers = [];

	send_dns(url, callback : Function)
	{
		this.http.get("http://" + config.dns_hostname + ":" + config.dns_port + "/dns?payload=" + url).subscribe(callback);
	}

	check_servers()
	{
		for(var index in this.servers)
		{
			var server = this.servers[index];
			this.http.get("http://" + server.ip + ":" + config.slicer_port + "/").subscribe(
				data => console.log(data),
				err => console.log(err),
				() => console.log("check_servers done")
			);
		}
		//console.log(JSON.stringify(this.servers));
		console.log("CHECK_SERVERS");
	}

	refresh_server_list(callback)
	{
		var _this = this;
		this.send_dns(config.slicer_hostname, function(res)
		{
			if(res.status == 200)
			{
				_this.servers = [];
	
				var servers = JSON.parse(res._body);
				for(var index in servers)
				{
					var server = servers[index];
					console.log("Server: " + server);
					_this.servers.push( { ip: server, name: 'No Status Yet', logo: 'fa-server', online_logo: 'fa-question' } );
				}
				console.log(res._body);
				callback();
			}
			console.log(JSON.stringify(res));
		});
		console.log("REFRESH_SERVER_LIST");
	}
		

     	constructor(http: Http)
	{
		console.log('dns_hostname: ' + config.dns_hostname);
		console.log('dns_port: ' + config.dns_port);

		var _this = this;
		_this.http = http;

		// Refresh the servers list (via. dns)
		setInterval(function() {
			_this.refresh_server_list(null);
		}, config.slicer_dns_recheck_frequency);
		
		// Ping all the servers
		setInterval(function() {
			_this.check_servers();
		}, config.slicer_ping_frequency);

		// Refresh the servers list now
		_this.refresh_server_list(function()
		{
			_this.check_servers();
		});
	}
}
