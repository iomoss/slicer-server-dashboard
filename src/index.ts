import {Component, View, bootstrap} from 'angular2/angular2';
import {HTTP_BINDINGS} from 'angular2/http';

import {Cluster} from './Cluster';

// Annotation section
@Component({
    selector: 'app'
})
@View({
    template: `
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <div class="navbar-header">
              <a class="navbar-brand" href="#">
                  <i class="fa fa-asterisk"></i>
                  Slicer-Server
              </a>
            </div>
          </div>
        </nav>
        <div>
	    <cluster />
        </div>
	<div>
Alfa
<!--
	    <cluster />
-->
	</div>
    `,
     directives: [Cluster]
})
// Component controller
class MyAppComponent { }

bootstrap(MyAppComponent, [HTTP_BINDINGS]);
